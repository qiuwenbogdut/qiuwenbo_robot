import numpy as np
import pyglet
import pandas as pd
import matlab.engine

class ArmEnv(object):
    action_bound = [-5, 5] #智能体动作的范围，-10°到10度之间
    state_dim = 13 #状态的维度

    action_dim = 3 # 机械臂动作的维度，这里也是三维，三个轴的转动量


    def __init__(self):

        self.on_goal = 0
        self.axis_move_degree=np.zeros(3,dtype=float) #该变量表示机械臂三个轴的转动量
        self.eng = matlab.engine.connect_matlab('fuck') #建立与matlab的联系


    def step(self, action):
        '''
        更新机械臂的状态和输出奖励
        :param action: 机械臂的动作
        :return: 返回三个参数，奖励，状态，是否完成
        '''
        done = False
        action = np.clip(action, self.action_bound[0],self.action_bound[1]) #将你的动作限定在一个合理范围内
        self.axis_move_degree+=action

        self.axis_move_degree[0]=np.clip( self.axis_move_degree[0],90,270)#限制各个轴的转动范围
        self.axis_move_degree[1]=np.clip( self.axis_move_degree[1],5,355)
        self.axis_move_degree[2]=np.clip( self.axis_move_degree[2],5,355)

        self.axis_move_degree=[one.item() for one in self.axis_move_degree] #由于matlab 不受支持的 Python 数据类型: numpy.float64



        [x3,y3,z3,x4,y4,z4,danger_status,move_status,distance_3,distance_4]=self.eng.DHfk3Dof_Lnya(self.axis_move_degree[0],\
                                                                          self.axis_move_degree[1],\
                                                                          self.axis_move_degree[2],0,nargout=10)
        distance_3=np.clip(distance_3,0,350)
        distance_4=np.clip(distance_4,0,350)
        reward=-0.5*np.sqrt(distance_4/350) #需要先对distance进行归一化处理

        reward+=

        # done and reward
        if danger_status==True or z4<0 or x4<0 : #如果发生碰撞，给予惩罚，并停止这个回合的训练,z的位置不能低于底座
            reward=-250.
            done=True
        else:
            if move_status==True:
                reward += 200.
                self.on_goal += 1
                if self.on_goal > 1:
                    done = True
            else:
                self.on_goal=0

            if self.on_goal:
                reward+=1.

        # state
        status=np.concatenate(([x3/100,y3/100,z3/100],[x4/100,y4/100,z4/100], \
                               [distance_3/100,distance_4/100], \
                               [1. if danger_status else 0.],\
                               [1. if self.on_goal else 0.],\
                               [one/350 for one in self.axis_move_degree]))
        #print(status)
        return status, reward, done

    def reset(self):
        '''
        重置环境,进行新的回合训练，重置机器人的状态，返回到起始点。
        机械臂的状态包括：机械臂末端所处的空间位置（x,y,x），机械臂末端距离目标的距离(d)，机械臂是否已经在目标附近(k)
        上述五个特征的量级不一样，需要记性归一化处理，会让训练收敛更快，
        :return: 返回的agent的状态
        '''

        self.axis_move_degree=[150.0,150.0,90.0] #定义机械臂的起点

        self.on_goal = 0

        [x3,y3,z3,x4,y4,z4,danger_status,move_status,distance_3,distance_4]=self.eng.DHfk3Dof_Lnya(self.axis_move_degree[0],\
                                                                  self.axis_move_degree[1],\
                                                                  self.axis_move_degree[2],1,nargout=10)

        status=np.concatenate(([x3/100,y3/100,z3/100],[x4/100,y4/100,z4/100], \
                               [distance_3/100,distance_4/100], \
                               [1. if danger_status else 0.],\
                               [1. if self.on_goal else 0.],\
                               [one/350 for one in self.axis_move_degree]))
        #todo 后面可以设置一个标志位来决定是否需要画出机械臂，以加快训练
        return status

    def render(self,axis_move_degree):
        '''
        将agent与环境互动的过程进行可视化
        :return:
        '''
        res=self.eng.DHfk3Dof_Lnya(axis_move_degree[0],axis_move_degree[1],axis_move_degree[2],0,nargout=10)
        return(res[6])

    def sample_action(self):
        '''
        two radians 生成[-0.5,0.5]之间的3个随机数
        三个轴的转动量
        :return:
        '''
        return (360*np.random.rand(3)-0.5).tolist()

    def stop_matlab_session(self):
        self.eng.quit()





if __name__ == '__main__':
    env = ArmEnv()
    # status=env.reset()
    # print(status)

    env.reset()
    res=[]
    for i in range(20):
        angle=env.sample_action()
        status=env.render(angle)
        angle.append(status)
        res.append(angle)


    res=pd.DataFrame(res)
    print(res)
    res.to_csv('suiji.csv')
    env.stop_matlab_session()
